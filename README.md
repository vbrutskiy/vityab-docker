# How to use
clone repo

git clone https://vbrutskiy@bitbucket.org/vbrutskiy/vityab-docker.git

cd vityab-docker

docker-compose up -d

# Centos 7 DEP
Docker dameon from official repo

Docker-compose from pip


# Prepare deps Centos 7

sudo yum install docker-engine

sudo tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

pip install --upgrade pip

pip install docker-compose

